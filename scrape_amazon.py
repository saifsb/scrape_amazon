from requests_html import HTMLSession
import pandas as pd

urls = ['https://www.amazon.co.uk/Safe-Travel-27852-Universal-Binding/dp/B08P24SDW6/ref=sr_1_1_sspa?dchild=1&keywords=car+mats&qid=1615548519&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFaV1kxVE5UOUdSSVEmZW5jcnlwdGVkSWQ9QTA1OTk2OTYzSVVaQ1Y3REZQV1BYJmVuY3J5cHRlZEFkSWQ9QTA1NTY4NTAyUDRPV0NQMzdDNU1CJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==',
          'https://www.amazon.co.uk/Samsung-TU7100-HDR-Smart-Tizen/dp/B086T39T5D/ref=sr_1_3?dchild=1&keywords=tv&qid=1601927782&refinements=p_n_size_browse-bin%3A9591881031&rnid=161398031&s=home-theater&sr=1-3',
          'https://www.amazon.co.uk/LG-75UN81006LB-Freeview-Freesat-built/dp/B0853M2XB1/ref=sr_1_7?dchild=1&keywords=tv&qid=1601927782&refinements=p_n_size_browse-bin%3A9591881031&rnid=161398031&s=home-theater&sr=1-7',
          'https://www.amazon.co.uk/notebook-computer-quad-core-celeron_j3455-processor-scalable/dp/B086WMLDVF/ref=sr_1_1_sspa?dchild=1&keywords=laptop&qid=1615548671&s=home-theater&sr=1-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExWlVOUDlHN0Q2N0M4JmVuY3J5cHRlZElkPUEwNzU1Njg3MkkxMDVFRUNRTEhCVSZlbmNyeXB0ZWRBZElkPUEwNDExMDQ5M0xHTDcxTlAwRjk2VSZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU=',
          'https://www.amazon.co.uk/Intel-i5-9400-processor-Smart-Cache-coffee/dp/B07MGZ9FJZ/ref=sr_1_3?dchild=1&keywords=i5+processor&qid=1615549197&sr=8-3',
          'https://www.amazon.co.uk/ASUS-B450-F-Gaming-Motherboard-Flashback/dp/B08K96RV9H/ref=sr_1_3?dchild=1&keywords=motherboard&qid=1615551618&sr=8-3']

def getPrice(url):
    s = HTMLSession()
    r = s.get(url)
    r.html.render(sleep=2)
    try:
        product = {
            'title': r.html.xpath('//*[@id="productTitle"]', first=True).text,
            'price': r.html.xpath('//*[@id="price_inside_buybox"]', first=True).text
            
            

        }
        print(product)
    except:
        product = {
            'title': r.html.xpath('//*[@id="productTitle"]', first=True).text,
            'price': 'price item unavailable'

        }
        print(product)
    return product

list_prices = []
for url in urls:
    list_prices.append(getPrice(url))

print(len(list_prices))

pricesdf = pd.DataFrame(list_prices)
pricesdf.to_excel('list_prices.xlsx', index=False)
